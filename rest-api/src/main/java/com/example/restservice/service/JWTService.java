package com.example.restservice.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

@Service
public class JWTService {

    private final static String KEY = "mySecretKey";

    public String getKey() {
        return KEY;
    }

    public String getJWTToken(String username) {

        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
            .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
            .builder()
            .setId("tokenId")
            .setSubject(username)
            .claim("authorities",
                grantedAuthorities.stream()
                                  .map(GrantedAuthority::getAuthority)
                                  .collect(Collectors.toList()))
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + 600000))
            .signWith(SignatureAlgorithm.HS512,
                KEY.getBytes()).compact();

        return "Bearer " + token;
    }
}
