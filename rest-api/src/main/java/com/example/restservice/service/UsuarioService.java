package com.example.restservice.service;

import com.example.restservice.dto.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UsuarioService {
    private List<Usuario> usuarios = new ArrayList<Usuario>();

    public void addUsuario(Usuario usuario) {
        usuarios.add(usuario);
    }

    public Usuario getUsuario(String usuario) {
        for (int i = 0; i < usuarios.size(); i++) {
            Usuario user = usuarios.get(i);
            if (user.getUsuario().equalsIgnoreCase(usuario)) {
                return user;
            }
        }
        return null;
    }

    public void createUsuario(String usuario, String password, String nombre, String apellido, String foto, Date fechaNacimiento) {
        Usuario user = new Usuario();
        user.setUsuario(usuario);
        user.setNombre(nombre);
        user.setApellido(apellido);
        user.setPassword(password);
        user.setFoto(foto);
        user.setFechaNacimiento(fechaNacimiento);
        addUsuario(user);
    }

    private boolean userExists(Usuario usuario) {
        if (usuario != null) {
            return true;
        }
        return false;
    }

    public void deleteUsuario(String usuario) {
        Usuario user = getUsuario(usuario);
        if (userExists(user)) {
            usuarios.remove(user);
        }
    }

    public void editarUsuario(Usuario usuario) {
        Usuario user = getUsuario(usuario.getUsuario());
        if (userExists(user)) {
            user.setPassword(usuario.getPassword() != null ? usuario.getPassword() : user.getPassword());
            user.setNombre(usuario.getNombre() != null ? usuario.getNombre() : user.getNombre());
            user.setApellido(usuario.getApellido() != null ? usuario.getApellido() : user.getApellido());
            user.setFoto(usuario.getFoto() != null ? usuario.getFoto() : user.getFoto());
            user.setFechaNacimiento(usuario.getFechaNacimiento() != null ? usuario.getFechaNacimiento() : user.getFechaNacimiento());
        }
    }

    public boolean isUsuarioValid(Usuario usuario) {
        if (usuario == null) {
            return false;
        } else {
            return (!StringUtils.isEmpty(usuario.getUsuario()) &&
                    !StringUtils.isEmpty(usuario.getPassword()) &&
                    !StringUtils.isEmpty(usuario.getNombre()) &&
                    !StringUtils.isEmpty(usuario.getApellido()) &&
                    !StringUtils.isEmpty(usuario.getFoto()) &&
                    !StringUtils.isEmpty(usuario.getFechaNacimiento()));
        }
    }
}
