package com.example.restservice.controller;

import com.example.restservice.dto.Usuario;
import com.example.restservice.dto.Login;
import com.example.restservice.service.JWTService;
import com.example.restservice.service.UsuarioService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

	private JWTService jwtService;
	private UsuarioService usuarioService;

	public UsuarioController(JWTService jwtService, UsuarioService usuarioService) {
		this.jwtService = jwtService;
		this.usuarioService = usuarioService;
	}

	@PutMapping("user")
	public Usuario create(@RequestBody Usuario usuario, HttpServletResponse response) throws IOException {
		if (usuarioService.isUsuarioValid(usuario)) {
			usuarioService.addUsuario(usuario);
			response.setStatus(HttpServletResponse.SC_CREATED);
			return usuario;
		} else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid user");
			return usuario;
		}
	}

	@GetMapping("user")
	public Usuario fetch(@RequestParam("user") String usuario, HttpServletResponse response) {
        Usuario user = usuarioService.getUsuario(usuario);
        if (user != null) {
            return user;
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}

	@DeleteMapping("user")
	public void delete(@RequestParam("user") String usuario, HttpServletResponse response) {
		usuarioService.deleteUsuario(usuario);
	}

	@PatchMapping("user")
	public Usuario edit(@RequestBody Usuario usuario, HttpServletResponse response) {
		usuarioService.editarUsuario(usuario);
		return usuarioService.getUsuario(usuario.getUsuario());
	}

	@PostMapping("user")
	public Login login(@RequestParam("user") String username, @RequestParam("password") String pwd,
						   HttpServletResponse response) {

		String token = jwtService.getJWTToken(username);
		Login user = new Login();
		user.setUsuario(username);
		user.setToken(token);
		return user;

        //boolean isValid = usuarioService.isUsuarioValid(usuarioService.getUsuario(usuario));
		//if (isValid) {
		//	String token = jwtService.getJWTToken(usuario);
		//	Login user = new Login();
		//	user.setUsuario(usuario);
		//	user.setToken(token);
		//	return user;
		//}

		//response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		//return null;
	}

}
